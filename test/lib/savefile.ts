import { describe, it, afterEach } from 'mocha';
import * as sinon from 'sinon';
import sinonChai from 'sinon-chai';
import fs from 'fs/promises';
import * as chai from 'chai';

chai.use(sinonChai);
const { expect } = chai;

import {
	// load,
	save,
	saveFile
} from '../../lib/savefile';
import { Fight, SavedFight } from '@blad-mercenary/core';

describe('savefile', (): void => {
	describe('save', () => {
		afterEach((): void => {
			sinon.restore();
		});

		it('convert to JSON and write the fight to disk', async (): Promise<void> => {
			sinon.stub(Fight, 'save').returns('saved' as unknown as SavedFight);
			sinon.stub(JSON, 'stringify').returns('stringified');
			sinon.stub(fs, 'writeFile').resolves();

			await save('fight' as unknown as Fight);

			expect(Fight.save).to.have.been.calledOnceWith('fight');
			expect(JSON.stringify).to.have.been.calledOnceWith('saved');
			expect(fs.writeFile).to.have.been.calledOnceWith(saveFile, 'stringified');
		});

		it('convert to JSON and write the fight to disk on custom location', async (): Promise<void> => {
			sinon.stub(Fight, 'save').returns('saved' as unknown as SavedFight);
			sinon.stub(JSON, 'stringify').returns('stringified');
			sinon.stub(fs, 'writeFile').resolves();

			await save('fight' as unknown as Fight, 'test location');

			expect(Fight.save).to.have.been.calledOnceWith('fight');
			expect(JSON.stringify).to.have.been.calledOnceWith('saved');
			expect(fs.writeFile).to.have.been.calledOnceWith('test location', 'stringified');
		});
	});
});
