import { Fight } from '@blad-mercenary/core';
export declare function newGame(): Promise<void>;
export declare function showStatus(fight: Fight): Fight;
export declare function showDetails(fight: Fight, characterId: string): Fight;
export declare function showNextCharacters(fight: Fight): Fight;
export declare function showActiveCharacter(fight: Fight): Fight;
export declare function showPossibleSelections(fight: Fight): Fight;
export declare function step(fight: Fight, selection: number): Fight;
export declare function showEvents(fight: Fight): Fight;
export declare function isEnded(fight: Fight): Fight;
