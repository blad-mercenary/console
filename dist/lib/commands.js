"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEnded = exports.showEvents = exports.step = exports.showPossibleSelections = exports.showActiveCharacter = exports.showNextCharacters = exports.showDetails = exports.showStatus = exports.newGame = void 0;
var core_1 = require("@blad-mercenary/core");
var savefile_1 = require("./savefile");
function newGame() {
    return __awaiter(this, void 0, void 0, function () {
        var characters, fight;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    characters = [
                        core_1.StandardCharacterType.goblin(core_1.Team.player),
                        core_1.StandardCharacterType.goblin(core_1.Team.enemy)
                    ];
                    fight = new core_1.Fight(characters);
                    console.log('Created a new fight');
                    return [4, savefile_1.save(fight)];
                case 1:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.newGame = newGame;
function showStatus(fight) {
    console.log("raw: " + JSON.stringify(fight) + "\n");
    console.log("fight id: " + fight.id);
    if (!fight.finished) {
        console.log('Active fight');
    }
    else {
        console.log("Fight finished and " + (fight.winner === core_1.Team.player ? 'won' : 'lost'));
    }
    var allies = fight.fighters.filter(function (fighter) { return fighter.team === core_1.Team.player; });
    var enemies = fight.fighters.filter(function (fighter) { return fighter.team === core_1.Team.enemy; });
    function format(character) {
        return [
            character.name + " (" + character.id + ")",
            "HP: " + character.health + " / " + character.maxHealth,
            "Mana: " + character.mana + " / " + character.maxMana,
            "Init: " + character.init
        ].join('\n');
    }
    console.log('\n');
    console.log("Allies:\n" + allies.map(function (ally) { return format(ally); }));
    console.log('\n -------------------------------------------- \n');
    console.log("Enemies:\n" + enemies.map(function (enemy) { return format(enemy); }));
    return fight;
}
exports.showStatus = showStatus;
function showDetails(fight, characterId) {
    var character = fight.fighters.find(function (fighter) {
        return fighter.id === characterId;
    });
    if (!character) {
        throw new Error('Invalid character id');
    }
    console.log("raw: " + JSON.stringify(character) + "\n");
    console.log(character.name + " (" + character.id + ")");
    console.log("HP: " + character.health + " / " + character.maxHealth);
    console.log("Mana: " + character.mana + " / " + character.maxMana);
    console.log("Init: " + character.init);
    var actions = character.actions.map(function (action) { return action.name + ": Cost " + action.mana + ", last " + action.time; });
    console.log("Actions:\n" + actions);
    return fight;
}
exports.showDetails = showDetails;
function showNextCharacters(fight) {
    var nextFighters = fight.getNextActiveFighters();
    console.log("raw: " + JSON.stringify(nextFighters) + "\n");
    console.log("Next:\n- " + nextFighters.map(function (fighter) { return fighter.name + " (" + fighter.id + ")"; }).join('\n- '));
    return fight;
}
exports.showNextCharacters = showNextCharacters;
function showActiveCharacter(fight) {
    var activeFighter = fight.getActiveFighter();
    console.log("raw: " + JSON.stringify(activeFighter) + "\n");
    console.log("Active fighter: " + activeFighter.name + " (" + activeFighter.id + ")");
    return fight;
}
exports.showActiveCharacter = showActiveCharacter;
function showPossibleSelections(fight) {
    var activeFighter = fight.getActiveFighter();
    var possibleSelections = core_1.getPossibleSelections(fight.getActiveFighter(), fight);
    console.log("raw: " + JSON.stringify(possibleSelections) + "\n");
    console.log("Possible selections for " + activeFighter.name + " (" + activeFighter.id + "):");
    possibleSelections.forEach(function (selection, i) {
        var targetNames = selection.targets.map(function (target) { return target.name + " (" + target.id + ")"; });
        var target;
        if (targetNames.length === 1) {
            target = targetNames[0];
        }
        else {
            target = "targetNames.slice(0, -1).join(', ')} and " + targetNames.slice(-1);
        }
        console.log("\t" + (i + 1) + ": Use " + selection.action.name + " on " + target);
    });
    return fight;
}
exports.showPossibleSelections = showPossibleSelections;
function step(fight, selection) {
    var time = new Date();
    fight.step(selection);
    var newEvents = fight.eventLogger.getEvents(time);
    console.log("raw: " + JSON.stringify(newEvents) + "\n");
    return fight;
}
exports.step = step;
function showEvents(fight) {
    console.log(fight.eventLogger.getEvents());
    return fight;
}
exports.showEvents = showEvents;
function isEnded(fight) {
    var playerWon = !fight.hasFighterAliveIn(core_1.Team.enemy);
    var enemyWon = !fight.hasFighterAliveIn(core_1.Team.player);
    if (playerWon) {
        console.log('You won');
    }
    else if (enemyWon) {
        console.log('You lost');
    }
    else {
        console.log('Still fighting');
    }
    return fight;
}
exports.isEnded = isEnded;
