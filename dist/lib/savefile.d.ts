import { Fight } from '@blad-mercenary/core';
export declare const saveFile = "./savefile.json";
export declare function load(file?: string): Promise<Fight>;
export declare function save(fight: Fight, file?: string): Promise<void>;
