"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var yargs_1 = __importDefault(require("yargs"));
var savefile_1 = require("../lib/savefile");
var commands_1 = require("../lib/commands");
function apply(callback) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return __awaiter(this, void 0, void 0, function () {
        var fight;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, savefile_1.load()];
                case 1:
                    fight = _a.sent();
                    return [4, callback.apply(void 0, __spreadArray([fight], args))];
                case 2:
                    fight = _a.sent();
                    return [4, savefile_1.save(fight)];
                case 3:
                    _a.sent();
                    return [2];
            }
        });
    });
}
yargs_1.default
    .command('new', 'create new game', {}, function () {
    return commands_1.newGame();
})
    .command('status', 'show status', {}, function () {
    return apply(commands_1.showStatus);
})
    .command('details [characterId]', 'show character status', {}, function (args) {
    return apply(commands_1.showDetails, args.characterId);
})
    .command('next', 'show the next characters that will play', {}, function () {
    return apply(commands_1.showNextCharacters);
})
    .command('active', 'show the active character', {}, function () {
    return apply(commands_1.showActiveCharacter);
})
    .command('possible', 'show action selection', {}, function () {
    return apply(commands_1.showPossibleSelections);
})
    .command('step [selectionId]', 'advance one turn', {}, function (args) {
    return apply(commands_1.step, Number.parseInt(args.selectionId) - 1);
})
    .command('events', 'show the event log', {}, function () {
    return apply(commands_1.showEvents);
})
    .command('ended', 'show if the fight ended', {}, function () {
    return apply(commands_1.isEnded);
})
    .demandCommand()
    .help()
    .argv;
