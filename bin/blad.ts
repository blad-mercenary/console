import yargs from 'yargs';
import { Fight } from '@blad-mercenary/core';
import { load, save } from '../lib/savefile';
import {
	newGame,
	showDetails,
	showStatus,
	showNextCharacters,
	showActiveCharacter,
	showPossibleSelections,
	step,
	showEvents,
	isEnded
} from '../lib/commands';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type FightCommand = (fight: Fight, ...args: any[]) => Fight | Promise<Fight>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function apply(callback: FightCommand, ...args: any[]): Promise<void> {
	let fight = await load();
	fight = await callback(fight, ...args);
	await save(fight);
}

yargs
	.command('new',
		'create new game',
		{},
		(): Promise<void> => {
			return newGame();
		})
	.command('status',
		'show status',
		{},
		(): Promise<void> => {
			return apply(showStatus);
		})
	.command('details [characterId]',
		'show character status',
		{},
		(args): Promise<void> => {
			return apply(showDetails, args.characterId);
		})
	.command('next',
		'show the next characters that will play',
		{},
		() => {
			return apply(showNextCharacters);
		})
	.command('active',
		'show the active character',
		{},
		() => {
			return apply(showActiveCharacter);
		})
	.command('possible',
		'show action selection',
		{},
		() => {
			return apply(showPossibleSelections);
		})
	.command('step [selectionId]',
		'advance one turn',
		{},
		(args): Promise<void> => {
			return apply(step, Number.parseInt(args.selectionId as string) - 1);
		})
	.command('events',
		'show the event log',
		{},
		() => {
			return apply(showEvents);
		})
	.command('ended',
		'show if the fight ended',
		{},
		() => {
			return apply(isEnded);
		})
	.demandCommand()
	.help()
	.argv;
