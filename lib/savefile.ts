import fs from 'fs/promises';
import { Fight, isSavedFight } from '@blad-mercenary/core';

export const saveFile = './savefile.json';

export async function load(file = saveFile): Promise<Fight> {
	const data = await fs.readFile(file, { encoding: 'utf-8' });
	const fightData = JSON.parse(data);
	if (!isSavedFight(fightData)) {
		throw new Error('Corrupted save file');
	}
	return Fight.load(fightData);
}

export async function save(fight: Fight, file = saveFile): Promise<void> {
	await fs.writeFile(file, JSON.stringify(Fight.save(fight)));
}