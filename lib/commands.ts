import { Character, Fight, StandardCharacterType, Team, getPossibleSelections } from '@blad-mercenary/core';
import { save } from './savefile';

export async function newGame(): Promise<void> {
	const characters = [
		StandardCharacterType.goblin(Team.player),
		StandardCharacterType.goblin(Team.enemy)
	];
	const fight = new Fight(characters);
	console.log('Created a new fight');
	await save(fight);
}

export function showStatus(fight: Fight): Fight {
	console.log(`raw: ${JSON.stringify(fight)}\n`);
	console.log(`fight id: ${fight.id}`);
	if (!fight.finished) {
		console.log('Active fight');
	} else {
		console.log(`Fight finished and ${fight.winner === Team.player ? 'won' : 'lost'}`);
	}

	const allies = fight.fighters.filter((fighter) => fighter.team === Team.player);
	const enemies = fight.fighters.filter((fighter) => fighter.team === Team.enemy);

	function format(character: Character): string {
		return [
			`${character.name} (${character.id})`,
			`HP: ${character.health} / ${character.maxHealth}`,
			`Mana: ${character.mana} / ${character.maxMana}`,
			`Init: ${character.init}`
		].join('\n');
	}

	console.log('\n');
	console.log(`Allies:\n${allies.map((ally) => format(ally))}`);
	console.log('\n -------------------------------------------- \n');
	console.log(`Enemies:\n${enemies.map((enemy) => format(enemy))}`);

	return fight;
}

export function showDetails(fight: Fight, characterId: string): Fight {
	const character = fight.fighters.find((fighter: Character): boolean => {
		return fighter.id === characterId;
	});

	if (!character) {
		throw new Error('Invalid character id');
	}

	console.log(`raw: ${JSON.stringify(character)}\n`);

	console.log(`${character.name} (${character.id})`);
	console.log(`HP: ${character.health} / ${character.maxHealth}`);
	console.log(`Mana: ${character.mana} / ${character.maxMana}`);
	console.log(`Init: ${character.init}`);
	const actions = character.actions.map(
		(action) => `${action.name}: Cost ${action.mana}, last ${action.time}`
	);
	console.log(`Actions:\n${actions}`);
	return fight;
}

export function showNextCharacters(fight:Fight): Fight {
	const nextFighters = fight.getNextActiveFighters();
	console.log(`raw: ${JSON.stringify(nextFighters)}\n`);
	console.log(`Next:\n- ${nextFighters.map((fighter) => `${fighter.name} (${fighter.id})`).join('\n- ')}`);
	return fight;
}

export function showActiveCharacter(fight:Fight): Fight {
	const activeFighter = fight.getActiveFighter();
	console.log(`raw: ${JSON.stringify(activeFighter)}\n`);
	console.log(`Active fighter: ${activeFighter.name} (${activeFighter.id})`);
	return fight;
}

export function showPossibleSelections(fight: Fight): Fight {
	const activeFighter = fight.getActiveFighter();
	const possibleSelections = getPossibleSelections(fight.getActiveFighter(), fight);
	console.log(`raw: ${JSON.stringify(possibleSelections)}\n`);
	console.log(`Possible selections for ${activeFighter.name} (${activeFighter.id}):`);
	possibleSelections.forEach((selection, i) => {
		const targetNames = selection.targets.map((target) => `${target.name} (${target.id})`);
		let target;
		if (targetNames.length === 1) {
			target = targetNames[0];
		} else {
			target = `targetNames.slice(0, -1).join(', ')} and ${targetNames.slice(-1)}`;
		}
		console.log(`\t${i + 1}: Use ${selection.action.name} on ${target}`);
	});
	return fight;
}

export function step(fight: Fight, selection: number): Fight {
	const time = new Date();
	fight.step(selection);
	const newEvents = fight.eventLogger.getEvents(time);
	console.log(`raw: ${JSON.stringify(newEvents)}\n`);
	return fight;
}

export function showEvents(fight: Fight): Fight {
	console.log(fight.eventLogger.getEvents());
	return fight;
}

export function isEnded(fight: Fight): Fight {
	const playerWon = !fight.hasFighterAliveIn(Team.enemy);
	const enemyWon = !fight.hasFighterAliveIn(Team.player);

	if (playerWon) {
		console.log('You won');
	} else if (enemyWon) {
		console.log('You lost');
	} else {
		console.log('Still fighting');
	}

	return fight;
}
